CREATE TABLE tags (
	id INTEGER PRIMARY KEY,
	name VARCHAR(128) UNIQUE
);
CREATE TABLE tag_relations (
	parent INTEGER REFERENCES tags(id),
	child INTEGER REFERENCES tags(id),

	PRIMARY KEY (parent, child)
);

CREATE TRIGGER bidirectional_check
BEFORE INSERT ON tag_relations
BEGIN
    SELECT RAISE(FAIL, 'that sucks')
	FROM tag_relations WHERE
		tag_relations.parent = NEW.child
		AND tag_relations.child = NEW.parent;
END;

INSERT INTO tags VALUES(NULL, 'go'), (NULL, 'programming'), (NULL, 'clang');
INSERT INTO tag_relations VALUES(
	(SELECT id FROM tags WHERE name = 'programming'),
	(SELECT id FROM tags WHERE name = 'clang')
);
INSERT INTO tag_relations VALUES(
	(SELECT id FROM tags WHERE name = 'programming'),
	(SELECT id FROM tags WHERE name = 'go')
);

