#!/usr/bin/env perl

use strict;
use warnings;
use feature 'say';
use DBI;
use autodie;

my $query = shift or die;
my $dbh   = DBI->connect("dbi:SQLite:dbname=tags.db");
my $sth   = $dbh->prepare(
    q|
WITH RECURSIVE
graph(id) AS (
	SELECT id FROM tags WHERE name = ?
	UNION ALL
	SELECT child
	FROM tag_relations, graph
	WHERE parent = id
)
SELECT name AS tag
FROM graph
JOIN tags ON tags.id = graph.id
|
);
$sth->execute($query);
while ( my $tag = $sth->fetchrow_array() ) {
    say $tag;
}
