#!/usr/bin/env bash

tags=$(./tags.pl "$@")
for tag in ${tags[@]}; do marks.pl get $tag; done | sort | uniq
